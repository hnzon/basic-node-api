const express = require("express");
const app = express();
const port = 3000;
const fs = require("fs");
const os = require("os");
const readline = require("readline");

app.get("/", (req, res) => {
  res.send("Hello World!");
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: `Choose an option:
  1. Read the package.json
  2. Display OS Info
  3. Start HTTP Server
  Type a number: `,
});

rl.prompt();
rl.on("line", (line) => {
  switch (line.trim()) {
    case "1":
      fs.readFile(__dirname + "/package.json", "utf-8", (err, content) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log(content);
      });
      rl.close();
      break;
    case "2":
      console.log(
        `Getting OS info...
        SYSTEM MEMORY: ${(os.totalmem() / 1024 / 1024 / 1024).toFixed(2)} GB
        FREE MEMORY: ${(os.freemem() / 1024 / 1024 / 1024).toFixed(2)} GB
        CPU CORES: ${os.cpus().length}
        ARCH: ${os.arch()}
        PLATFORM ${os.platform()}
        RELEASE: ${os.release()}
        USER: ${os.userInfo().username}`
      );
      rl.close();
      break;
    case "3":
      app.listen(port, () => {
        console.log(`listening on port: ${port}`);
      });
      break;
    default:
      console.log("Invalid option");

      rl.prompt();
      break;
  }
  rl.prompt();
}).on("close", () => {
  process.exit(0);
});
